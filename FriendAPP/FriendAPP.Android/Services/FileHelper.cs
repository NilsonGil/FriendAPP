﻿using System;
using FriendAPP.Services;
using FriendAPP.Droid.Services;
using Xamarin.Forms;
using System.IO;

[assembly: Dependency(typeof(FileHelper))]
namespace FriendAPP.Droid.Services
{
    public class FileHelper : IFileHelper
    {

        public FileHelper()
        {
                
        }
        public string GetLocalFilePath(string fileName)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            return Path.Combine(path, fileName);
        }
    }
}