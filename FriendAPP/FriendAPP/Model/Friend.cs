﻿using System;
using System.Collections.Generic;
using System.Text;

using FriendAPP.ViewModel;
using SQLite;

namespace FriendAPP.Model
{
    public class Friend:Notificable
    {

        #region Atributos
            [PrimaryKey,AutoIncrement]
            public int ID { get; set; }
            private string nombre;
            private string telefono;
            private string correo;
        #endregion
        #region Propiedades


        public string Correo
        {
            get { return correo; }
            set {
                SetValue(ref correo, value);
            }
        }


        public string Telefomo
        {
            get { return telefono; }
            set
            {
                SetValue(ref telefono, value);
            }
        }

        public string Nombre
        {
            get { return nombre; }
            set
            {
                SetValue(ref nombre, value);
            }
        }
        #endregion
    }
}
